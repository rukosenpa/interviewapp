from python:3.9.2

RUN apt-get update

# установка protobuf
RUN apt-get install -y golang
RUN apt-get install -y protobuf-compiler
RUN go get -u github.com/verloop/twirpy/protoc-gen-twirpy
ENV PATH=$PATH:/root/go/bin/:/root/.local/bin:/home/vizorlabs/.local/bin

COPY requirements.txt ./
RUN pip install -r requirements.txt

RUN mkdir /opt/app

COPY ./src/ /opt/app

WORKDIR /opt/app

CMD uvicorn server:app --host 0.0.0.0 --port=3000
