# client.py
from twirp.context import Context
from twirp.exceptions import TwirpServerException

from src.protobuf import services_pb2, services_twirp

client = services_twirp.DollarClient("http://localhost:3000")

# if you are using a custom prefix, then pass it as `server_path_prefix`
# param to `MakeHat` class.
try:
    response = client.getValue(ctx=Context(), request=services_pb2.Empty())
    print(response)
except TwirpServerException as e:
    print(e.code, e.message, e.meta, e.to_dict())
