# server.py

import xml.etree.ElementTree as ET

from aiohttp import ClientSession
from twirp.asgi import TwirpASGIApp

from protobuf import services_pb2, services_twirp


class GetDollarService(object):
    session = ClientSession()

    @staticmethod
    def parse_currency_value(data, value_id='R01235'):
        root = ET.fromstring(data)
        value = root.find(f"Valute[@ID='{value_id}']/Value").text.replace(',', '.')

        return float(value)

    async def getValue(self, context, _empty):
        response = await self.session.get('http://www.cbr.ru/scripts/XML_daily.asp')

        data = await response.text()

        dollar_value = self.parse_currency_value(data, 'R01235')

        return services_pb2.Currency(
            value=dollar_value
        )


# if you are using a custom prefix, then pass it as `server_path_prefix`
# param to `HaberdasherServer` class.
service = services_twirp.DollarServer(service=GetDollarService())
app = TwirpASGIApp()
app.add_service(service)
